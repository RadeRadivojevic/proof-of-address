const fbClient = require('./fb_client');
const {TransactionOperation, PeerType, TransactionStatus} = require("fireblocks-sdk");
const util = require("util");

/*
* Create a transaction with a typed message
* The message format is {vasp_prefix}{address}{UUID}
* vasp_prefix: an arbitrary string when the receiving VASP claimed custody of an
* address (stored in BB)
* address: the address of the wallet
* UUID: the address registration UUID when the receiving VASP claimed custody of an address
*
* docs: https://developers.fireblocks.com/docs/typed-message-signing
*/
async function signArbitraryMessage(vaultAccountId, message, asset, addressIndex = 0) {

    const { status, id } = await fbClient.createTransaction({
        operation: TransactionOperation.TYPED_MESSAGE,
        assetId: asset,
        source: {
            type: PeerType.VAULT_ACCOUNT,
            id: vaultAccountId
        },
        note: `Test Message`,
        extraParameters: {
            rawMessageData: {
                messages: [{
                    content: message,
                    index: addressIndex,
                    type: "BTC_MESSAGE"
                }]
            }
        }
    });
    let currentStatus = status;
    let txInfo;

    if(currentStatus === TransactionStatus.COMPLETED) {
        console.log("Transaction Status: ", currentStatus);
        txInfo = await fbClient.getTransactionById(id);
    }

    while (currentStatus !== TransactionStatus.COMPLETED && currentStatus !== TransactionStatus.FAILED) {
        console.log("keep polling for tx " + id + "; status: " + currentStatus);
        txInfo = await fbClient.getTransactionById(id);
        currentStatus = txInfo.status;
        await new Promise(r => setTimeout(r, 1000));
    };

    const signature = txInfo.signedMessages[0].signature;

    console.log(JSON.stringify(signature));

    const encodedSig = Buffer.from([Number.parseInt(signature.v,16) + 31]).toString("hex") + signature.fullSig;
    console.log("Encoded Signature:", Buffer.from(encodedSig,"hex").toString("base64"));
}

(async () => {
    const vaultAccountId = 0;
    const asset = "BTC_TEST";
    const walletAddresses = await fbClient.getDepositAddresses(vaultAccountId, asset);

    console.log("PUB_____KEY_______: ", walletAddresses);

    if (walletAddresses.length === 0) {
        throw new Error("No wallet addresses found");
    }
    // message format is {vasp_prefix}{address}{UUID}
    const message = "tripleaio" + walletAddresses[0].address + "975f0090-a88f-4be0-a123-d38484e8394d";
    await signArbitraryMessage("0", message, asset);
})();