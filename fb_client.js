const fs = require("fs");
const path = require("path");

const FireblocksSDK = require("fireblocks-sdk").FireblocksSDK
require('dotenv').config();




function addEnvironmentVariables() {

    let serverAddress, userId, privateKey;
    // Parse command-line arguments
    const args = process.argv.slice(2);

    // Initialize default values
    let environment;

    // Loop through the arguments and extract the values
    args.forEach((arg) => {
        if (arg.startsWith('--environment=')) {
            environment = arg.split('=')[1];
        }
    });

    if (!environment) {
        serverAddress = process.env.SERVER_ADDRESS;
        userId = process.env.USER_ID;
        privateKey = fs.readFileSync(path.resolve(__dirname, 'KEYS/', "env.keys"), "utf8");
    } else if (environment === 'production') {
        serverAddress = process.env.SERVER_ADDRESS_PROD;
        userId = process.env.USER_ID_PROD;
        privateKey = fs.readFileSync(path.resolve(__dirname, 'KEYS/', "pro.keys"), "utf8");
        console.log(`Using private key: ${privateKey}`);
    } else if (environment === 'staging') {
        serverAddress = process.env.SERVER_ADDRESS_STAGING;
        userId = process.env.USER_ID_STAGING;
        privateKey = fs.readFileSync(path.resolve(__dirname, 'KEYS/', "env.keys"), "utf8");
    } else if (environment === 'pre1') {
        serverAddress = process.env.SERVER_ADDRESS_PRE_1;
        userId = process.env.USER_ID_PRE_1;
        privateKey = fs.readFileSync(path.resolve(__dirname, 'KEYS/', "pre1.keys"), "utf8");
    } else if (environment === 'local') {
        serverAddress = process.env.SERVER_ADDRESS_LOCAL;
        userId = process.env.USER_ID_LOCAL;
        privateKey = fs.readFileSync(path.resolve(__dirname, 'KEYS/', "local.keys"), "utf8");
    }
    else {
        throw new Error(`Unknown environment: ${environment}`);
    }

    if(!serverAddress || !userId || !privateKey) {
        throw new Error(`Missing environment variables. Please make sure you have the following environment variables set: SERVER_ADDRESS, USER_ID, PRIVATE_KEY`);
    }


    return {
        serverAddress,
        userId,
        privateKey,
    }
}

const { serverAddress, userId, privateKey } = addEnvironmentVariables();

fireblocks = new FireblocksSDK(privateKey, userId, serverAddress, undefined, {
    customAxiosOptions: {
        interceptors: {
            response: {
                onFulfilled: (response) => {
                    console.log(`Request ID: ${response.headers["x-request-id"]}`);
                    return response;
                },
                onRejected: (error) => {
                    if (error.response && error.response.headers && error.response.headers["x-request-id"]) {
                        console.log(`Request ID: ${error.response.headers["x-request-id"]}`);
                    }
                    // console.log(`Request ID: ${error.response.headers["x-request-id"]}`);
                    throw error;
                }
            }
        }
    },
    travelRuleOptions: {
        authURL: process.env.NOTABENE_AUTH_URL,
        baseURL: process.env.NOTABENE_BASE_URL,
        audience: process.env.NOTABENE_AUDIENCE,
        baseURLPII: process.env.NOTABENE_BASE_URL_PII,
        audiencePII: process.env.NOTABENE_AUDIENCE_PII,
        clientId: process.env.NOTABENE_CLIENT_ID,
        clientSecret: process.env.NOTABENE_CLIENT_SECRET,
        jsonDidKey: process.env.JSON_DID_KEY,
    }
});

module.exports = {
    async getVASPList(filter) {
        return await fireblocks.getAllTravelRuleVASPs(filter)
    },

    async getVASPDetails(vaspDid) {
        return await fireblocks.getTravelRuleVASPDetails(vaspDid)
    },

    async createTransaction(transaction) {
        return await fireblocks.createTransaction(transaction, undefined, {
                beneficiaryPIIDidKey: undefined,
                sendToProvider: false
            }
        )
    },

    async getTransaction(transactionId) {
        return await fireblocks.getTransactionById(transactionId)
    },

    async validateTravelRuleVASP(transactionVaspDetails) {
        return await fireblocks.validateTravelRuleTransaction(transactionVaspDetails)
    },

    async validateTravelRuleFull(transaction) {
        return await fireblocks.validateFullTravelRuleTransaction(transaction)
    },

    async getTravelRulePostScreeningPolicy() {
        return await fireblocks.getTravelRulePostScreeningPolicy()
    },

    async getTravelRuleScreeningPolicy() {
        return await fireblocks.getTravelRuleScreeningPolicy()
    },

    async getTravelRuleScreeningConfiguration() {
        return await fireblocks.getTravelRuleScreeningConfiguration()
    },

    async setTravelRuleBypassConfiguration(configuration) {
        return await fireblocks.updateTravelRulePolicyConfiguration(configuration)
    },

    async getTransactionById(id) {
        return await fireblocks.getTransactionById(id)
    },

    async getDepositAddresses(vaultAccountId, assetId) {
        return await fireblocks.getDepositAddresses(vaultAccountId, assetId)
    },

    async getAssets() {
        return await fireblocks.getSupportedAssets()
    }
};